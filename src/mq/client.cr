require "amqp-client"

module Finex::MQ
  module Client
    @@connection : ::AMQP::Client::Connection?
    @@channel : ::AMQP::Client::Channel?
    @@exchanges = Hash(String, ::AMQP::Client::Exchange).new

    def self.connection
      options = {
        host:     ENV["RABBITMQ_HOST"] || "0.0.0.0",
        port:     ENV["RABBITMQ_PORT"] || "5672",
        username: ENV["RABBITMQ_USERNAME"],
        password: ENV["RABBITMQ_PASSWORD"],
      }

      @@connection ||= ::AMQP::Client.new(host: options["host"], port: options["port"].to_i, user: options["username"], password: options["password"]).connect
    end

    def self.channel
      @@channel ||= Client.connection.channel
    end

    def self.exchange(name : String, type = "topic", durable = false)
      @@exchanges[name] ||= channel.exchange(name: name, type: type, durable: durable)
    end

    def self.publish(name : String, type : String, id : String, event : String, payload)
      routing_key = [type, id, event].join(".")
      serialized_data = payload.to_json
      exchange(name).publish(message: serialized_data, routing_key: routing_key)
      Finex.logger.debug { "published event to #{routing_key} " }
    end

  end
end
